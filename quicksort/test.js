const quicksort = require("./index");

test("quicksort function exists", () => {
  expect(quicksort).toBeDefined();
});

test("quicksort handles empty arrays as an input", () => {
  expect(quicksort([])).toEqual([]);
});

test("quicksort sort a Array", () => {
  expect(quicksort([4, 9, 2, 1, 6, 3, 8])).toEqual([1, 2, 3, 4, 6, 8, 9]);
});
