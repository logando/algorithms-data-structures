//quicksort([4, 9, 2, 1, 6, 3, 8])
function quicksort(arr) {
  if (arr.length < 1) {
    return [];
  }

  let pivot = arr[0];
  let left = [];
  let right = [];

  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < pivot) {
      left.push(arr[i]);
    } else {
      right.push(arr[i]);
    }
  }
  return [].concat(quicksort(left), pivot, quicksort(right));
}

module.exports = quicksort;
