const util = require('util');
const fs = require('fs');


const fetchArrayFromFile = async (ruteFile) =>{
    try{
        let result = [];
        fs.readFileSync= util.promisify(fs.readFile);
        const dataFromFile = await fs.readFileSync(ruteFile);
        const str = dataFromFile.toString();
        const lines = str.split('\r\n');
        
    
        lines.map(line =>{
            if(!line){return null}
            result.push(Number(line));
        });
       // console.log("result",result)
        return result;
    }catch(err){
        console.error(err);
    }

}

module.exports = fetchArrayFromFile;