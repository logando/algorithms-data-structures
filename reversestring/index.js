// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

//First Solution
//function reverse(str) {
//    return str.split('').reverse().join('');
//}

// function reverse(str){
//     let reversed ='';
//     for(let ch of str){
//         reversed =  ch + reversed;
//     }
//     return reversed;
// }

function reverse(str){
    return str.split('').reduce((acum,curr) => curr+acum,'');
}

module.exports = reverse;
