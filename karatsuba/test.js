const karatsuba = require('./index');

test('karatsuba function exists', () => {
  expect(karatsuba).toBeDefined();
});

test('karatsuba handles 0 as an input', () => {
    expect(karatsuba(0,100)).toEqual(0);
  });
  
  test('karatsuba flips a positive number', () => {
    expect(karatsuba(5,5)).toEqual(25);
    expect(karatsuba(15,5)).toEqual(75);
  });